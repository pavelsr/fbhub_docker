# You can pass target ubuntu and perl versions using docker --build-args
# time docker build --build-arg UBUNTU_VERSION=18.04 PERL_VERSION=5.28.0 -t pavelsr/xxxhub .

ARG UBUNTU_VERSION=16.04

FROM ubuntu:${UBUNTU_VERSION}
RUN cat /etc/lsb-release

LABEL maintainer "Pavel Serikov <pavelsr@cpan.org>"

RUN apt-get update && \
    apt-get -y install \
    build-essential \
    curl \ 
    apache2 \
    libperl-dev \
    mysql-client \
    libmysqlclient-dev \
    libgmp-dev \
    libssl-dev \
    libexpat-dev \
    libxslt-dev \
    libdb-dev
    
# libperl-dev ( or libperl5.22 ) - Cwd for installing perlbrew

ENV PERLBREW_ROOT /usr/local/perlbrew
ENV PATH /usr/local/perlbrew/bin:$PATH
RUN curl -L https://install.perlbrew.pl | bash

ARG PERL_VERSION=5.30.0
RUN perlbrew install perl-${PERL_VERSION}
ENV PATH ${PERLBREW_ROOT}/perls/perl-$PERL_VERSION/bin:$PATH
RUN perl -e 'print $^V'

RUN curl -fsSL --compressed https://git.io/cpm > /usr/local/bin/cpm && \
    chmod +x /usr/local/bin/cpm
    
# turboxsl
RUN apt-get -y install git autoconf automake libtool libck-dev libgettextpo-dev libmemcached-dev && \
    git clone https://github.com/Litres/turboxsl.git && \
    cd turboxsl && \
    autoreconf -ivf && \
    aclocal && \
    autoheader && \
    autoconf && \
    automake --add-missing && \
    ./configure --prefix=/opt/turboxsl/ --exec-prefix=/opt/turboxsl/ && \
    # cp libtool libtool.bak && \
    # cp /usr/bin/libtool . && \
    make && \
    make install

# libmaxminddb
RUN curl -L https://github.com/maxmind/libmaxminddb/releases/download/1.3.2/libmaxminddb-1.3.2.tar.gz -o libmaxminddb-1.3.2.tar.gz && \
    tar xzpf ./libmaxminddb-1.3.2.tar.gz && \
    cd libmaxminddb-1.3.2/ && \
    ./configure && \ 
    make && \
    make install && \
    ldconfig

# can not just `cpm install -g TurboXSL` since MetaCPAN has non-latest version of TurboXSL
RUN git clone https://github.com/Litres/TurboXSLT && \
    cd TurboXSLT && \
    perl Makefile.PL && \
    make && \
    make install

# Memcached::libmemcached configures ~5 min
RUN cpm install -gv --configure-timeout=300 Cache::Memcached::libmemcached
COPY cpanfile ./
RUN cpm install -gv

#RUN cpm install -g CGI Carp CGI::Carp
#RUN cpm install -g Crypt::Rijndael_PP
#RUN cpm install -g Switch


# RUN perlbrew list-modules