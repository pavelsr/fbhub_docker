#!/usr/bin/env bash
# Script generate two cpanfiles

CWD=$(pwd)
PATH_TO_TRUNK=/data/FictionHub_trunk
CPANFILE="${CWD}/cpanfile"

cd ${PATH_TO_TRUNK}
scan-prereqs-cpanfile --ignore=bin,tools,work > ${CPANFILE}

sed -i "/requires 'XPortal/d" ${CPANFILE}
sed -i "/requires 'Posrednik/d" ${CPANFILE}
sed -i "/requires 'ServerID/d" ${CPANFILE}
sed -i "/requires 'cron::/d" ${CPANFILE}
sed -i "/requires 'work::/d" ${CPANFILE}

# модули, которые нужно ставить ручками
# можно отключить еще Alien::SVN, долго ставится
sed -i "/requires 'Image::Magick/d" ${CPANFILE}
sed -i "/requires 'PerlMagick/d" ${CPANFILE}
sed -i "/requires 'TurboXSLT/d" ${CPANFILE}
sed -i "/requires 'Net::HandlerSocket/d" ${CPANFILE}

# --configure-timeout=120
sed -i "/requires 'Cache::Memcached/d" ${CPANFILE}
sed -i "/requires 'Inline::Java/d" ${CPANFILE}
sed -i "/requires 'CataLITTest/d" ${CPANFILE}
sed -i "/requires 'EnvHub/d" ${CPANFILE}
sed -i "/requires 'FB2ToTXT/d" ${CPANFILE}
sed -i "/requires 'FB3::OPCNode/d" ${CPANFILE}
sed -i "/requires 'Filesys::DfXPortal::ForkMe/d" ${CPANFILE}
sed -i "/requires 'Framework::/d" ${CPANFILE}
sed -i "/requires 'ImportArt/d" ${CPANFILE}
sed -i "/requires 'MachineCPPSock/d" ${CPANFILE}
sed -i "/requires 'MachineHub/d" ${CPANFILE}
sed -i "/requires 'MachineValidate/d" ${CPANFILE}
sed -i "/requires 'MoneyHolderTest/d" ${CPANFILE}
sed -i "/requires 'SystemHub/d" ${CPANFILE}
sed -i "/requires 'TelegramBot/d" ${CPANFILE}
sed -i "/requires 'Text::Distiler/d" ${CPANFILE}
sed -i "/requires 'UpdatePdfTest/d" ${CPANFILE}
sed -i "/requires 'bookconverter/d" ${CPANFILE}
sed -i "/requires 'cleanFB2_light/d" ${CPANFILE}
sed -i "/requires 'next_chk/d" ${CPANFILE}
sed -i "/requires 'Win32/d" ${CPANFILE}

sed -i "/requires 'SVN::Look/d" ${CPANFILE}
sed -i "/requires 'File::List/d" ${CPANFILE}
sed -i "/requires 'WWW::Curl/d" ${CPANFILE}

# scan-prereqs-cpanfile
# Distribution does not have META.json nor META.yml
# https://github.com/Litres/TurboXSLT
# cpm install -gv git://github.com/Litres/TurboXSLT.git
# scan-prereqs-cpanfile --ignore=bin,cron,tools,work
# Perl::PrereqScanner

# non-working alternatives of scan-prereqs-cpanfile
# https://metacpan.org/pod/distribution/Perl-PrereqScanner/bin/scan_prereqs
# https://metacpan.org/pod/distribution/Perl-PrereqScanner/bin/scan-perl-prereqs
