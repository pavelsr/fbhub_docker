requires 'Algorithm::LUHN';
requires 'Amazon::S3';
requires 'Archive::Zip';
requires 'Business::Barcode::EAN13';
requires 'CAM::PDF';
requires 'Carp';
requires 'CGI';
requires 'CGI::Carp';
requires 'CGI::Header';
requires 'CGI::Session';
requires 'Class::Date';
requires 'Clone';
requires 'Compress::Zlib';
requires 'Config::Any';
requires 'Config::MySQL::Reader';
requires 'Crypt::CBC';
requires 'Crypt::JWT';
requires 'Crypt::OpenSSL::RSA';
requires 'Crypt::Random';
requires 'Crypt::Rijndael_PP';
requires 'DBD::mysql';
requires 'DBI';
requires 'DB_File';
requires 'DDP';
requires 'Data::Dump';
requires 'Data::Dumper::AutoEncode';
requires 'Data::Entropy::RawSource::Local';
requires 'Data::Entropy::Source';
requires 'Data::MessagePack';
requires 'Data::Printer';
requires 'Data::UUID';
requires 'Date::Format';
requires 'Date::Manip';
requires 'Date::Manip::Date';
requires 'Date::Manip::Delta';
requires 'Date::Parse';
requires 'DateTime';
requires 'Digest::JHash';
requires 'Digest::MD5';
requires 'Digest::MD5::File';
requires 'Digest::SHA';
requires 'Encode';
requires 'Excel::Writer::XLSX';
requires 'FB3';
requires 'FB3::Validator';
requires 'File::Copy::Recursive';
requires 'File::Find::Rule';
requires 'File::Path';
requires 'File::ReadBackwards';
requires 'File::ShareDir';
requires 'File::Slurp';
requires 'File::Temp';
requires 'File::chdir';
requires 'Font::TTF::Font';
requires 'Geo::IP';
requires 'Getopt::Long';
requires 'Google::Ads::AdWords::Client';
requires 'Google::Ads::AdWords::Logging';
requires 'Google::Ads::AdWords::v201309::AdGroup';
requires 'Google::Ads::AdWords::v201309::AdGroupAd';
requires 'Google::Ads::AdWords::v201309::AdGroupAdOperation';
requires 'Google::Ads::AdWords::v201309::AdGroupCriterionOperation';
requires 'Google::Ads::AdWords::v201309::AdGroupOperation';
requires 'Google::Ads::AdWords::v201309::BiddableAdGroupCriterion';
requires 'Google::Ads::AdWords::v201309::CpcBid';
requires 'Google::Ads::AdWords::v201309::Keyword';
requires 'Google::Ads::AdWords::v201309::Money';
requires 'Google::Ads::AdWords::v201309::Paging';
requires 'Google::Ads::AdWords::v201309::RelatedToQuerySearchParameter';
requires 'Google::Ads::AdWords::v201309::TargetingIdeaSelector';
requires 'Google::Ads::AdWords::v201309::TextAd';
requires 'Google::Ads::AdWords::v201809::AdGroup';
requires 'Google::Ads::AdWords::v201809::AdGroup::Status';
requires 'Google::Ads::AdWords::v201809::AdGroupAd';
requires 'Google::Ads::AdWords::v201809::AdGroupAdOperation';
requires 'Google::Ads::AdWords::v201809::AdGroupCriterionOperation';
requires 'Google::Ads::AdWords::v201809::AdGroupOperation';
requires 'Google::Ads::AdWords::v201809::AttributeType';
requires 'Google::Ads::AdWords::v201809::BiddableAdGroupCriterion';
requires 'Google::Ads::AdWords::v201809::BiddingStrategyConfiguration';
requires 'Google::Ads::AdWords::v201809::CpcBid';
requires 'Google::Ads::AdWords::v201809::ExemptionRequest';
requires 'Google::Ads::AdWords::v201809::ExpandedTextAd';
requires 'Google::Ads::AdWords::v201809::IdeaType';
requires 'Google::Ads::AdWords::v201809::Keyword';
requires 'Google::Ads::AdWords::v201809::KeywordMatchType';
requires 'Google::Ads::AdWords::v201809::Money';
requires 'Google::Ads::AdWords::v201809::Operator';
requires 'Google::Ads::AdWords::v201809::Paging';
requires 'Google::Ads::AdWords::v201809::PolicyViolationError';
requires 'Google::Ads::AdWords::v201809::PolicyViolationKey';
requires 'Google::Ads::AdWords::v201809::RelatedToQuerySearchParameter';
requires 'Google::Ads::AdWords::v201809::RequestType';
requires 'Google::Ads::AdWords::v201809::TargetingIdeaSelector';
requires 'Google::Ads::AdWords::v201809::TextAd';
requires 'Google::Ads::AdWords::v201809::UserStatus';
requires 'Google::Ads::Common::ErrorUtils';
requires 'Google::Ads::Common::MapUtils';
requires 'HTML::LinkExtor';
requires 'HTTP::Cookies';
requires 'HTTP::Date';
requires 'HTTP::Headers';
requires 'HTTP::Lite';
requires 'HTTP::Request';
requires 'HTTP::Request::Common';
requires 'HTTP::Response';
requires 'HTTP::Server::Simple::CGI';
requires 'Hash::Merge';
requires 'IO::Compress::Zip';
requires 'IO::Socket::SSL';
requires 'IO::Socket::Timeout';
requires 'IO::Uncompress::Gunzip';
requires 'IO::Uncompress::Unzip';
requires 'IO::Zlib';
requires 'Image::ExifTool';
requires 'Image::Size';
requires 'Inline';
requires 'JSON', '2.53';
requires 'JSON::PP';
requires 'JSON::Path';
requires 'JSON::WebToken';
requires 'JSON::XS';
requires 'JSONY';
requires 'JavaScript::Minifier';
requires 'LWP';
requires 'LWP::MediaTypes';
requires 'LWP::Simple';
requires 'LWP::UserAgent';
requires 'Lingua::Identify';
requires 'List::Util';
requires 'Locale::PO', '0.21';
requires 'Locale::TextDomain';
requires 'Locale::Util';
requires 'Log::Dispatch';
requires 'Log::Log4perl';
requires 'Log::Log4perl::Level';
requires 'MIME::Base64';
requires 'MIME::EncWords';
requires 'MIME::Lite';
requires 'MIME::QuotedPrint';
requires 'MIME::Words';
requires 'MRO::Compat';
requires 'MYDan::Util::Sudo';
requires 'Math::Random';
requires 'MaxMind::DB::Reader::XS';
requires 'Memcached::libmemcached';
requires 'Memory::Usage';
requires 'Module::Load';
requires 'Moo';
requires 'Moo::Role';
requires 'Net::FTP';
requires 'Net::Netmask';
requires 'Net::OAuth2::Profile::WebServer';
requires 'Net::OpenID::Consumer';
requires 'Net::SFTP::Foreign', '1.67';
requires 'Net::SSL';
requires 'Net::Server';
requires 'Net::Server::Fork';
requires 'Net::Server::PreFork';
requires 'Net::Server::SIG';
requires 'Net::Syslog';
requires 'Net::Twitter::Lite::WithAPIv1_1';
requires 'Number::Bytes::Human';
requires 'OPC';
requires 'PPI::Dumper';
requires 'PPI::XS';
requires 'Parallel::ForkManager';
requires 'Perl::Builtins';
requires 'PerlIO::gzip';
requires 'Pod::Simple::HTML';
requires 'Pod::Simple::HTMLBatch';
requires 'Pod::Text::Color';
requires 'Pod::Usage';
requires 'Redis';
requires 'Role::Tiny';
requires 'Roman';
requires 'SOAP::Lite';
requires 'SVN::Client';
requires 'SVN::Wc';
requires 'Scalar::Util';
requires 'Set::IntSpan';
requires 'Socket';
requires 'Spreadsheet::ParseExcel';
requires 'Statistics::RankCorrelation';
requires 'Statistics::Test::WilcoxonRankSum';
requires 'Sys::HostIP';
requires 'Switch';
requires 'TAP::Harness';
requires 'Term::ANSIColor';
requires 'Term::ProgressBar';
requires 'Term::ReadKey';
requires 'Test::Builder';
requires 'Test::Class';
requires 'Test::Deep';
requires 'Test::Exception';
requires 'Test::More';
requires 'Test::More::UTF8';
requires 'Test::WWW::Selenium';
requires 'Text::CSV';
requires 'Text::Distill';
requires 'Text::Patch';
requires 'Text::Unidecode';
requires 'Time::Local';
requires 'Time::Piece';
requires 'UNIVERSAL::require';
requires 'URI';
requires 'URI::Escape';
requires 'URI::QueryParam';
requires 'URI::URL';
requires 'WWW::Robot';
requires 'WWW::Telegram::BotAPI';
requires 'XML::LibXML';
requires 'XML::LibXSLT';
requires 'XML::Parser';
requires 'XML::Simple';
requires 'XML::Twig';
requires 'autovivification';
requires 'feature';
requires 'lib::abs';
requires 'mro';
requires 'parent';
requires 'perl', 'v5.16.0';
